# Kubernetes test application deployment

This is *Kubernetes* deployment performed in *pure K8S* way - no *helm* nor other deployment managers is required.  
This stack creates *Notejam* application environment to easily test ability to spawn workload on *Kubernetes* cluster.  

> **Notice**  
> This project was created for demo purposes and should be treated as **PoC** only.  

## What is deployed  
  
Stack deployed using this repository creates:  

- **namespace** to store workload in a logically separated space  
- **default limit settings** to set expected limit for all containers  
- **persistent volumes** located locally on *Kubernetes* workers  
- **persistent volume claims** to automatically grab *persistent volumes*  
- **deployment** in *high availability* standard  
- **autoscaler** to guarantee application usage comfort  
- **network policy** to limit access to workload  

## *pure K8S* way limitations

*pure K8S* way has some limitations which can be omitted using deployment manager eg. [Helm](https://helm.sh/).  
Those limitations are:  

- hardcoded values  
- lack of variables to parametrize stack quickly  
- order of *Kubernetes* entities has to be maintained via files order  

> **Notice**  
> This stack can be easily transformed into [Helm](https://helm.sh/) chart.  

## How to deploy

To deploy stack run following command:

```bash
kubectl apply -f ./notejam-application-deployment
```

## Possible improvements aka. TODO list  

In next demo stages some improvements may be apply to this stack:

- transform to [Helm](https://helm.sh/) chart  
- add `securityContext` for `pods`  
- harden network policies
- add `podAntiAffinity` and `podAffinity`  
- expose workload over `ingress` or external loadbalancer
